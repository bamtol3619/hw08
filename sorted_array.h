#include <iostream>

#ifndef __hw08__sorted_array__
#define __hw08__sorted_array__

using namespace std;

template <typename T>
class SortedArray {
 public:
  SortedArray(){
	size_=0;
	alloc_=0;
    values_ = NULL;
  }
  SortedArray(const SortedArray& a){
	size_=a.size();
	alloc_=a.alloc();
	values_ = new T[size_];
	for(int i=0; i<alloc_; ++i){
		values_[i] = a(i);
	}
  }
  SortedArray(size_t size){
	size_=size;
	alloc_=0;
	values_ = new T[size];
  }
  ~SortedArray() { delete[] values_; }

  SortedArray& operator=(const SortedArray& a){
	size_ = a.size();
	alloc_ = a.alloc();
	T* temp = new T[a.size()];
	for(int i=0; i<a.alloc(); ++i)	temp[i] = a(i);
	delete[] values_;
	values_ = temp;
  }

  size_t size() const { return size_; }
  size_t alloc() const { return alloc_; }
  const T& operator()(int idx) const {
	if(idx>=0 && idx<alloc_)	return values_[idx];
  }

  void Reserve(int size){  // 주어진 크기만큼 미리 메모리 할당.
		values_ = new T[size];
		size_ = size;
  }
  void Add(const T& value) {
	values_[alloc_++] = value;
  }

  int Find(const T& value){// 주어진 값의 위치, 없으면 -1을 리턴. Binary Search를 사용해야 함
    int start = 0, end=size_-1;
    for(int i=0; i<10; ++i){
        if(end==size_-1 && start==end-1) start = end;
        if(value > values_[(start+end)/2])  start = (start+end)/2;
        else if(value < values_[(start+end)/2]) end = (start+end)/2;
        else return (start+end)/2;
    }
	return -1;
  }
  void Sort(){
    T temp;
    for(int i=0; i<size_-1; ++i){
        for(int j=i+1; j<size_; ++j){
            if(values_[i]>values_[j]){
                temp = values_[i];
                values_[i] = values_[j];
                values_[j] = temp;
            }
        }
    }
  }

 private:
  T* values_;
  int size_, alloc_;
};

template <typename T>
istream& operator>>(istream& is, SortedArray<T>& a){
	int size;
	is >> size;
	a.Reserve(size);
	for(int i=0; i<size; ++i){
		T temp;
		is >> temp;
		a.Add(temp);
	}
	a.Sort();
	return is;
}

template <typename T>
ostream& operator<<(ostream& os, const SortedArray<T>& a){
	for(int i=0; i<a.alloc(); ++i)	os << a(i) << " ";
	return os;
}

#endif