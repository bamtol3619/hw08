#include <iostream>
#include <vector>

#ifndef __hw08__new_vector__
#define __hw08__new_vector__

using namespace std;

union NewType{
    int int_;
    double double_;
    char char_;
};

class NewVector
{
public:
    NewVector(){count_ = 0;}
    template <typename T> void add(T _value){
		NewType value;
		if(sizeof(_value) == sizeof(int)){
			value.int_ = _value;
			used_.push_back('i');
		}
		else if(sizeof(_value) == sizeof(double)){
			value.double_ = _value;
			used_.push_back('d');
		}
		else if(sizeof(_value) == sizeof(char)){
			value.char_ = _value;
			used_.push_back('c');
		}
		values_.push_back(value);
		++count_;
	}
    inline int count(){return count_;}
    
    friend ostream& operator << (ostream& _os, const NewVector _nv);
    
private:
    int count_;
    vector<NewType> values_;
	vector<char> used_;
};

ostream& operator << (ostream& _os, const NewVector _nv){
	for(int i=0; i<_nv.count_; ++i){
			if(_nv.used_[i] == 'i')	_os << _nv.values_[i].int_ << " ";
			else if(_nv.used_[i] == 'd')	_os << _nv.values_[i].double_ << " ";
			else if(_nv.used_[i] == 'c')	_os << _nv.values_[i].char_ << " ";
		}
		_os << endl;
		return _os;
}

#endif